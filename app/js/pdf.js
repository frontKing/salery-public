
// If absolute URL from the remote server is provided, configure the CORS
// header on that server.
var url = window.location.origin + '/pdf/test.pdf';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

var pdfDoc = null,
    pageNum = 1,
    pageRendering = false,
    pageNumPending = null,
    scale = 2,
    canvas = document.getElementById('pdf'),
    ctx = canvas.getContext('2d'),
    i = 1;

/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */
function renderPage(num) {
  pageRendering = true;
  // Using promise to fetch the page
  pdfDoc.getPage(num).then(function(page) {
    var viewport = page.getViewport({scale: scale});
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: ctx,
      viewport: viewport
    };
    var renderTask = page.render(renderContext);

    // Wait for rendering to finish
    renderTask.promise.then(function() {
      pageRendering = false;
      if (pageNumPending !== null) {
        // New page rendering is pending
        renderPage(pageNumPending);
        pageNumPending = null;
      }
    });
  });

}

function renderPageList(maxPages) {
    pageRendering = true;
    // Using promise to fetch the page
    for (i = 1; i < maxPages+1; i++) {
        pdfDoc.getPage(i).then(function(page) {
            if ( page.pageIndex+1 == pageNum ) {
                $('.instruction-list').append('<li class="active"><canvas></canvas></li>')
            } else {
                $('.instruction-list').append('<li><canvas></canvas></li>')
            }
            canvasItem = $('.instruction-list li:last-child canvas')[0]
            var viewport = page.getViewport({scale: scale});
            canvasItem.height = viewport.height;
            canvasItem.width = viewport.width;
            myCtx = canvasItem.getContext('2d');

            // Render PDF page into canvas context
            var renderContext = {
              canvasContext: myCtx,
              viewport: viewport
            };
            var renderTask = page.render(renderContext);
        
          });
    }


}

/**
 * If another page rendering in progress, waits until the rendering is
 * finised. Otherwise, executes rendering immediately.
 */
function queueRenderPage(num) {
  if (pageRendering) {
    pageNumPending = num;
  } else {
    renderPage(num);
  }
}

/**
 * Displays previous page.
 */
function onPrevPage() {
  if (pageNum <= 1) {
    return;
  }
  $('.instruction-list li').removeClass('active');
  $('.instruction-list li').eq(pageNum-2).addClass('active');
  pageNum--;
  queueRenderPage(pageNum);
}
document.getElementById('prev').addEventListener('click', onPrevPage);

/**
 * Displays next page.
 */
function onNextPage() {
  if (pageNum >= pdfDoc.numPages) {
    return;
  }
  $('.instruction-list li').removeClass('active');
  $('.instruction-list li').eq(pageNum).addClass('active');
  pageNum++;
  queueRenderPage(pageNum);
}
document.getElementById('next').addEventListener('click', onNextPage);

/**
 * Asynchronously downloads PDF.
 */
pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
  pdfDoc = pdfDoc_;
  // Initial/first page rendering
  renderPage(pageNum);
  renderPageList(pdfDoc.numPages);
});


$('body').on('click', '.instruction-list li', function() {
    $('.instruction-list li').removeClass('active');
    $(this).addClass('active');
    queueRenderPage($('.instruction-list li.active').index()+1);
    pageNum=$('.instruction-list li.active').index()+1;
  });

