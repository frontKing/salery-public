(function() {
  // var nbaTeams = new Bloodhound({
  //   datumTokenizer: Bloodhound.tokenizers.obj.whitespace('team'),
  //   queryTokenizer: Bloodhound.tokenizers.whitespace,
  //   prefetch: '../data/nba.json'
  // });

  let iPhones = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('team'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: '../data/telephone.json'
  });

  $('.typeahead-header').typeahead(
    {
      highlight: true
    },
    // {
    //   name: 'nba-teams',
    //   display: 'team',
    //   source: nbaTeams,
    //   templates: {
    //     header: Handlebars.compile('<div class="model-name">iPhone123</div>'),
    //     suggestion: Handlebars.compile('<a href="#" class="model-name">{{team}}</a>'),
    //   }
    // },
    {
      name: 'iPhones',
      display: 'team',
      source: iPhones,
      templates: {
        header: Handlebars.compile('' +
          '<div class="header-ajax-search">' +
          '<div class="header-ajax-search-name">Категории</div>' +
          '<div class="header-ajax-search-category">iPhone</div>' +
          '</div>' +
          '<div class="header-ajax-search-name">Модели</div>'),
        suggestion: Handlebars.compile('<a href="#" class="header-model-name">{{team}}</a>'),
      }
    }
  );

  /*header-navigation*/
  /*lvl 1 header-list*/
  $('.burger').on('click', function(e) {
    e.stopPropagation();
    const menuWrapper = $(this).next();
    const menu = $(this).next().find('.header-list');
    if (menuWrapper.hasClass('active')) {
      menuWrapper.removeClass('active');

      $(this).find('.burger__line:first').removeClass('rotate-line_first');
      $(this).find('.burger__line:last').removeClass('rotate-line_last');

      $(this).css('height', '100%');
      $(this).css('max-width', '100%');
      $(this).removeClass('burger-padding');

      $(this).closest('.header-nav').prev('.header-part').find('.header-authorise-mobile').css('display', 'flex');
      $(this).closest('.header-nav').prev('.header-part').find('.header-authorise-desktop').css('display', 'none');

      if ($(window).width() < 992) {
        $('.header-filter').show();
      }

      if ($(window).width() > 767) {
        $('.header-search').css('transform', 'unset');
      }
      
      menu.removeClass('container');
      $('body').css('overflow', 'auto');

      $('.sub-nav').removeClass('active');
    } else {
      menuWrapper.addClass('active');
      menu.addClass('container');

      $(this).find('.burger__line:first').addClass('rotate-line_first');
      $(this).find('.burger__line:last').addClass('rotate-line_last');

      $(this).css('max-width', '50%');
      $(this).css('height', 'auto');
      $(this).addClass('burger-padding');

      $(this).closest('.header-nav').prev('.header-part').find('.header-authorise-mobile').css('display', 'none');
      $(this).closest('.header-nav').prev('.header-part').find('.header-authorise-desktop').css('display', 'flex');

      if ($(window).width() < 992) {
        $('.header-filter').hide();
      }
      
      if (($(window).width() > 767) && ($(window).width() < 992)) {
        $('.header-search').css('transform', 'translateX(-25px)');
      }

      $('body').css('overflow', 'hidden');

      /*promocodes-bags*/
      $('.header-filter').removeClass('active');
      if ($(window).width() < 992) {
        $('.catalog-sidebar').hide(300);
      }
      /*promocodes-bags*/
    }
  });
  /*lvl 2 sub-nav*/
  $('body').on('click', '.header-list__link', function(e) {

    if ($(window).width() < 1290) {
      /*tablet*/
      e.preventDefault();
      const subNav = $(this).next();
      if (subNav.hasClass('active')) {
        subNav.removeClass('active');
      } else {
        subNav.addClass('active');
      }
    }
  });
  /*back to lvl 1 menu*/
  $('.link-back').on('click', function(e) {
    e.preventDefault();
    $('.header-list__link').next().removeClass('active');
  });

  /*767 lvl 3*/
  function menuLevelThree() {
    if ($(window).width() < 768) {
      $('.sub-list__item.first').on('click', function() {
        let li = $(this).parent().find('li:not(:eq(0))');
        let liFirst = $(this).parent().find('.first');

        li.toggle(300);
        liFirst.find('i').toggleClass('rotate-icon');
      });
    }
  }
  menuLevelThree();
  /*767 lvl 3*/

  /*close mob menu */
  $('.main, .breadcrumbs-wrapper, .header-part, .helper-menu, .header-list-close').on('click', function(e) {
    const menuWrapper = $('.header-list-wrapper');
    const menu = $('.header-list');
    // e.stopPropagation();
    if (menuWrapper.hasClass('active')) {
      menuWrapper.removeClass('active');

      $('.burger').find('.burger__line:first').removeClass('rotate-line_first');
      $('.burger').find('.burger__line:last').removeClass('rotate-line_last');

      $('.burger').css('height', '100%');
      $('.burger').css('max-width', '100%');
      $('.burger').removeClass('burger-padding');

      $('.burger').closest('.header-nav').prev('.header-part').find('.header-authorise-mobile').css('display', 'flex');
      $('.burger').closest('.header-nav').prev('.header-part').find('.header-authorise-desktop').css('display', 'none');

      if ($(window).width() < 992) {
        $('.header-filter').show();
      }
      if ($(window).width() > 767) {
        $('.header-search').css('transform', 'unset');
      }
      menu.removeClass('container');
      $('body').css('overflow', 'auto');

      $('.sub-nav').removeClass('active');
    }
  });
  /*close mob menu */

  /*promocodes*/
  $('.menu-categories__item').hover(function() {
    $('.tab-content').hide();
    $(".menu-categories__item").removeClass('active');
    $(this).addClass("active");
    // $(this).find('a').preventDefault();
    let selected_tab = $(this).find('a').attr("href");
    $(selected_tab).stop().show();
    return false;
  });
  /*promocodes*/

  /*header-filter*/
  $('.header-filter').on('click', function() {

    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('.catalog-sidebar').hide(300);
      $('body').css('overflow', 'auto');
    } else {
      $(this).addClass('active');
      $('.catalog-sidebar').show(300);
      $('body').css('overflow', 'hidden');
    }

    /*burger bags*/
    $('.header-list-wrapper').removeClass('active');
    $('.sub-nav').removeClass('active');
    const burger = $('.burger');
    burger.css('max-width', '100%');
    burger.css('height', '100%');
    burger.find('.burger__line:first').removeClass('rotate-line_first');
    burger.find('.burger__line:last').removeClass('rotate-line_last');
    burger.removeClass('burger-padding');
    $('.header-authorise-mobile').css('display', 'flex');
    $('.header-authorise-desktop').css('display', 'none');
    /*burger bags*/
  });
  /*header-filter*/

  /*input search*/
  $('.header-search').on('click', '.header-search__input_icon', function() {
    if ($(window).width() < 768) {
      const headerSearch = $(this).closest('.header-search');
      if (headerSearch.hasClass('active')) {
        headerSearch.removeClass('active');
        // headerSearch.find('.header-search__input').hide();
        $(this).removeClass('filter-icon-unwhite');
      } else {
        headerSearch.addClass('active');
        // headerSearch.find('.header-search__input').show();
        $(this).addClass('filter-icon-unwhite');
      }
    }
  });
  /*input search*/

  /*/header-navigation*/

  /*header-region*/
  $('.header-region').magnificPopup({
    type: 'inline',
    preloader: true,
    midClick: true,
    closeBtnInside: true,
    callbacks: {
      open: function() {
        $('.body').addClass('body-region');
      },
      close: function (){
        $('.body').removeClass('body-region');
      }
    }
  });
  /*header-region*/


  /*resize*/
  // window.addEventListener("resize", function() {
  //   $('body').css('overflow', 'auto');
  //   /*burger bags*/
  //   $('.header-list-wrapper').removeClass('active');
  //   $('.sub-nav').removeClass('active');
  //   const burger = $('.burger');
  //   burger.css('max-width', '100%');
  //   burger.css('height', '100%');
  //   burger.find('.burger__line:first').removeClass('rotate-line_first');
  //   burger.find('.burger__line:last').removeClass('rotate-line_last');
  //   burger.removeClass('burger-padding');
  //   if ($(window).width() < 1289) {
  //     $('.header-authorise-mobile').css('display', 'flex');
  //     $('.header-authorise-desktop').css('display', 'none');
  //   } else {
  //     $('.header-authorise-desktop').css('display', 'flex');
  //     $('.header-authorise-mobile').css('display', 'none');
  //   }
  //   /*burger bags*/
  //
  //   /*promocodes-bags*/
  //   $('.header-filter').removeClass('active');
  //   if ($(window).width() < 992) {
  //     $('.catalog-sidebar').hide(300);
  //   } else {
  //     $('.catalog-sidebar').show(300);
  //   }
  //   /*promocodes-bags*/
  // }, false);
  /*resize*/
})();