$(function () {
  $('.coop').slick({
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 1,
    arrow: true,
    responsive: [
      {
        breakpoint: 1290,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 361,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true
        }
      }
    ]
  });
  $('.coop').on('setPosition', function () {
    $(this).find('.slick-slide').height('auto');
    var slickTrack = $(this).find('.slick-track');
    var slickTrackHeight = $(slickTrack).height();
    $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
  });

  $('.about-write').slick({
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 1,
    arrow: true,
    responsive: [
      {
        breakpoint: 1290,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 361,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true
        }
      }
    ]
  });
  $('.about-write').on('setPosition', function () {
    $(this).find('.slick-slide').height('auto');
    var slickTrack = $(this).find('.slick-track');
    var slickTrackHeight = $(slickTrack).height();
    $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
  });

  $('.main-image-list-slider').slick({
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 1,
    arrow: true,
    responsive: [
      {
        breakpoint: 1290,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 361,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true
        }
      }
    ]
  });
  $('.main-image-list-slider').on('setPosition', function () {
    $(this).find('.slick-slide').height('auto');
    var slickTrack = $(this).find('.slick-track');
    var slickTrackHeight = $(slickTrack).height();
    $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
  });


  function ratingSlider(myClass) {
    myClass = myClass || '.rating-slider';
    $(myClass).each(function() {
      var slideCol = $(this).attr('data-col');
      if (!slideCol) {
        slideCol = 3;
      }

      if ( slideCol <= 3) {
        $(this).slick({
          infinite: true,
          slidesToShow: slideCol,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
          responsive: [
            {
              breakpoint: 1290,
              settings: {
                slidesToShow: slideCol-1,
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
              }
            },
          ]
        });
      } else {
        $(this).slick({
          infinite: true,
          slidesToShow: slideCol,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
          responsive: [
            {
              breakpoint: 1290,
              settings: {
                slidesToShow: slideCol-1,
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
              }
            },
          ]
        });
      }
    });
    
  }
  /* ratingSlider(); */
  
  $('.promos-slider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 1290,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 450,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  });	  

  $(".slider").bootstrapSlider({
    formatter: function(value) {
      if(Array.isArray(value)) {
        $(".slider-min").text(value[0] + ' р.');
        $(".slider-max").text(value[1] + ' р.');
      }
    }
  });
  $(".js-slider-catalog").on("slide", function(slideEvt) {
    $(".slider-min").text(slideEvt.value[0] + ' р.');
    $(".slider-max").text(slideEvt.value[1] + ' р.');
  });

  function promocodeSlider() {
    $('.promocodes-slider').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      dots: true,
      responsive: [
        {
          breakpoint: 1290,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 450,
          settings: {
            slidesToShow: 1,
          }
        },
      ]
    });
    $('.promocodes-slider').on('init', function(event, slick, currentSlide, nextSlide){
      console.log('resize');
    });
  }
  promocodeSlider();

  $('.expired-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 1290,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 450,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  });

  $('.shopping-slider').slick({
    infinite: true,
    arrows: false,
    dots: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1290,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 450,
        settings: {
          arrows: true,
          dots: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
    ]
  });

  if ($(".about-me__text").length) {
    $(".about-me__text").mCustomScrollbar();
  }

  if ($(".instruction-scroll").length) {
    $(".instruction-scroll").mCustomScrollbar({
      axis:"x"
    });
  }

  $('.list-select li').on('click', function() {
    var select = $(this).closest('.list-select');
    select.find('li').removeClass('active');
    $(this).addClass('active');
    select.find('.list-select__title').html($(this).html());
  });
  $('.list-select').on('click', function() {
    $(this).toggleClass('active');
  });

  $('.catalog-filter__title').on('click', function() {
    $(this).next('.catalog-filter__filter').slideToggle(400);
    $(this).toggleClass('active');
  });

  $('.section-close').on('click', function() {
    $(this).next('.section-main').slideToggle(400);
    $(this).toggleClass('active');
    $(this).next('.section-main').toggleClass('active');
  });

  $('.top-brands__list li').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.top-brands__list').find('li').removeClass('active');
    $(this).addClass('active');
    $(this).closest('section').find('.top-brands__list-change .top-brands-items').removeClass('active');
    $(this).closest('section').find('.top-brands__list-change .top-brands-items').eq($(this).index()).addClass('active');
  });

  $('.list-select li').on('click', function() {
    $(this).closest('ul').find('li').removeClass('active');
    $(this).addClass('active');
    $(this).closest('section').find('.rating-slider').removeClass('active');
    $(this).closest('section').find('.list-select-slider > div').eq($(this).index()).addClass('active');
    $('.list-select-slider > div.active').slick('destroy');
    ratingSlider('.list-select-slider > div.active');
  });

  function promocodesWidgetSlider() {
    $('.promocodes-widget-list__item').on('click', function() {
      $(this).closest('ul').find('li').removeClass('active');
      $(this).addClass('active');
      $(this).closest('section').find('.promocodes-item').removeClass('active');
      $(this).closest('section').find('.promocodes-item').eq($(this).index()).addClass('active');
      $('.promocodes-slider').slick('destroy');

      promocodeSlider();

      promocodesModal();
      promocodesStockModal();
    });
  }
  promocodesWidgetSlider();
  promocodesModal();
  promocodesStockModal();

  window.addEventListener("resize", function() {
    setTimeout(function() {
      promocodesModal();
      promocodesStockModal();
    }, 100);
  });
  function promocodesModal() {
    $('.promocodes-slider__more .btn').magnificPopup({
      type: 'inline',
      preloader: true,
      midClick: true,
      closeBtnInside: true,
      callbacks: {
        open: function() {
          $('.body').addClass('body-promocodes');
        },
        close: function () {
          $('.body').removeClass('body-promocodes');
        }
      }
    });
  }
  function promocodesStockModal() {
    $('.done-text').magnificPopup({
      items: {
        src: '#promocodes-stock-modal',
        type: 'inline'
      },
      callbacks: {
        open: function() {
          $('.body').addClass('body-promocodes');
        },
        close: function () {
          $('.body').removeClass('body-promocodes');
        }
      }
    });
  }


  if ($('select').length) {
    $('select').styler();
  }

  $('.pa-pause').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    $(this).closest('.pa-sale-row').toggleClass('disabled');
  });

  $('.articles-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 1290,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 450,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  });

  $('.widget-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
  });	  

  $('.content-slider').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    responsive: [
      {
        breakpoint: 450,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  });	  

  $('.similar-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
      {
        breakpoint: 1290,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 560,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  });	  

  $('.profitable-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    responsive: [
      {
        breakpoint: 1290,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  });	  

  if ($('.catalog-lists').length) {
    $('.catalog-lists').masonry({
      itemSelector: '.catalog-list'
    });
  }

  $('.product-slider__main').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.product-slider__small'
  });
  $('.product-slider__small').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    vertical: true,
    arrows: false,
    asNavFor: '.product-slider__main',
    dots: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 767,
        settings: {
          verticalSwiping: false,
          vertical: false,
          slidesToShow: 5,
        }
      },
    ]
  });
    
  $('.promos-slider-vertical').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    vertical: true,
    arrows: true,
    dots: false,
    centerMode: false,
    verticalSwiping: true,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          verticalSwiping: false,
          vertical: false,
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 767,
        settings: {
          verticalSwiping: false,
          vertical: false,
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 500,
        settings: {
          verticalSwiping: false,
          vertical: false,
          slidesToShow: 2,
        }
      },
    ]
  });
  $('.promos-slider').on('setPosition', function() {
    $(this).find('.slick-slide').height('auto');		      
    var slickTrack = $(this).find('.slick-track');		      
    var slickTrackHeight = $(slickTrack).height();		      
    $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');		      
  });

  $('.related-ratings .rating-list').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrow: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true
        }
      }
    ]
  });

  function promosSliderVertical() {
    if( $(window).width() <= 991 ) {
      $('.promos-slider-vertical').on('setPosition', function () {		      
        $(this).find('.slick-slide').height('auto');		      
        var slickTrack = $(this).find('.slick-track');		      
        var slickTrackHeight = $(slickTrack).height();		      
        $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');		      
      });	
    } else {
      $('.promos-slider-vertical').on('setPosition', function () {		      
        $(this).find('.slick-slide').height('auto');		      	      
      });	
    }
  }
  promosSliderVertical();

  // function ratingListSlider(){
  //   if( $(window).width() <= 1289 ) {
  //     $('.rating-list-slider').on('setPosition', function () {
  //       $(this).find('.slick-slide').height('auto');
  //       var slickTrack = $(this).find('.slick-track');
  //       var slickTrackHeight = $(slickTrack).height();
  //       $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
  //     });
  //   } else {
  //     $('.promos-slider-vertical').on('setPosition', function () {
  //       $(this).find('.slick-slide').height('auto');
  //     });
  //   }
  // }
  // ratingListSlider();

  $(window).resize(function(){
    promosSliderVertical();
    footerBottom();
    // ratingListSlider();
  });

  // Animation on click
  // $('.product-tabs .product-tabs__item').on('click', function() {
  //   $('.product-tabs .product-tabs__item').removeClass('active');
  //   $(this).addClass('active');
  //   $('.product-tabs-show .product-tabs-show__item').removeClass('active');
  //   $('.product-tabs-show .product-tabs-show__item').eq($(this).index()).addClass('active');
  //   $('.instruction-slider').slick('destroy');
  //   $('.instruction-slider').slick({
  //     infinite: false,
  //     slidesToShow: 1,
  //     slidesToScroll: 1,
  //     arrows: true,
  //     dots: false,
  //   });
  // });

  $('.similar__list li').on('click', function(){
    $(this).closest('.similar__container').find('.similar__list li').removeClass('active');
    $(this).addClass('active');
    $(this).closest('.similar__container').find('.similar__items .similar__items-list').removeClass('active');
    $(this).closest('.similar__container').find('.similar__items .similar__items-list').eq($(this).index()).addClass('active');
  });

  $('.top-rating-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 1290,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
  });

  function footerBottom() {
    $('body').css('padding-bottom', $('footer').innerHeight());
  }
  footerBottom();

  /*validation*/
  function validLk(form) {
    let error = false;
    let errorMessageText = '';
    form.find('.lk-valid-mess').remove();
    form.find('input').each(function() {
      $(this).css('border-color', 'transparent');
      if ($(this).attr('type') === 'email') {
        error = validateEmail($(this).val());
        if (error) {
          errorMessageText = 'Email не прошел валидацию';
          errorMessage($(this), errorMessageText);
        }
      }
      else if ($(this).hasClass('pass-confirm')) {
        if ($(this).val() === '') {
          errorMessageText = 'Заполните поле';
          errorMessage($(this), errorMessageText);
        } else if ($(this).val() !== $('.pass').val()) {
          errorMessageText = 'Пароли не совпадают';
          errorMessage($(this), errorMessageText);
        } else {
          $(this).css('border-color', 'transparent');
        }
      }
      else {
        if ($(this).val() === '' && ($(this).is(':required'))) {
          errorMessageText = 'Заполните поле';
          errorMessage($(this), errorMessageText);
        }
      }
    });
  }
  function errorMessage(input, errorMessageText) {
    input.css('border-color', '#cd0031');
    input.next().append("<span class='lk-valid-mess'>" + errorMessageText + "</span>");
    input.next().show(600);
    input.css('margin-bottom', '0');
  }
  function validateEmail($email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return !regex.test($email);
  }
  $('form').submit(function(e) {
    e.preventDefault();
    validLk($(this));
  });
  // $('#registre-form-modal').submit(function(e) {
  //   e.preventDefault();
  //   validLk($(this));
  // });
  // $('#lk-registration').submit(function(e) {
  //   e.preventDefault();
  //   validLk($(this));
  // });
  /*/validation*/

  if(!!$('.product-main-section')) {
    var productPageAnchorTop = $('.js-product-page_anchor')[0].offsetTop;
    window.addEventListener('scroll', function() {
      window.scrollY > productPageAnchorTop
        ? $('.product-main-section').addClass('scroll')
        : $('.product-main-section').removeClass('scroll')
    });
  }


  $('.catalog-widget.rating').css('opacity', '1');
  $('.promocodes-widget').css('opacity', '1');
  $('.top-rating-slider').css('opacity', '1');
  $('.profitable-slider').css('opacity', '1');
  $('.product-slider__main').css('opacity', '1');
  $('.product-slider__small').css('opacity', '1');
  $('.rating-slider.active').css('opacity', '1');
  $('.list-select-slider .slick-dotted.slick-slider').css('opacity', '1');
  $('.image-grid-section_slider .image-grid').css('opacity', '1');
  $('.promos-slider').css('opacity', '1');
  $('.promos-slider-vertical').css('opacity', '1');
  $('.content-slider').css('opacity', '1');
  $('.similar-slider').css('opacity', '1');
  $('.profitable-cost-form').css('opacity', '1');

  if(window.innerWidth <= 991) {
    $('.product').on('click', function(e) {
      $(this).toggleClass('active')
    })
  }
});