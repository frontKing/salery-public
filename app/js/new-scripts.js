/*function sales modal on hover*/
let flag = false; /*флаг для модалки*/
function tabHover() {
  $(document).mousemove(function(e) {

    if (!$('body').hasClass('body-for-helper')) {
      if (e.pageY <= 5) {
        // Launch MODAL BOX
        if (!flag) {
          $.magnificPopup.open({
            items: {
              src: '#sales-modal',
              type: 'inline'
            },
            callbacks: {
              open: function() {
                $('body').addClass('body-sales');
                $("#sales-slider").bootstrapSlider("refresh");
              },
              close: function () {
                $('body').removeClass('body-sales');
                flag = true;
              }
            }
          });
        }
      }
    }

  });
}
/*function sales modal on hover*/
let _window = $(window);
$(document).ready(function() {
  /*main-page second section*/
  $('.image-grid-section .image-grid').slick({
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 1,
    arrow: true,
    responsive: [
      {
        breakpoint: 1290,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 361,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true
        }
      }
    ]
  });
  $('.image-grid-section .image-grid').on('setPosition', function () {
    $(this).find('.slick-slide').height('auto');
    var slickTrack = $(this).find('.slick-track');
    var slickTrackHeight = $(slickTrack).height();
    $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
  });
  /*main-page second section*/

  /* main-page business slider */
  function businessSliderActivated() {
    if (_window.width() < 768) {
      $('.business-list').not('.slick-initialized').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrow: true,
        adaptiveHeight: true
      });
    } else {
      if ($('.business-list').hasClass('slick-initialized')) {
        $('.business-list').slick('destroy');
      }
    }
  }
  businessSliderActivated();
  window.addEventListener("resize", function() {
    businessSliderActivated();
  });
  /* main-page business slider */

  /*main-page tabs*/
  $('.buying-process .list-white li').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.list-white').find('li').removeClass('active');
    $(this).addClass('active');
    $(this).closest('section').find('.buying-process-list .buying-process-item').removeClass('active');
    $(this).closest('section').find('.buying-process-list .buying-process-item').eq($(this).index()).addClass('active');
  });
  /*main-page tabs*/

  /*main-page feedback-section section*/
  function sliderActivated() {
    if ($(window).width() < 768) {
      $('.feedback-list-slick').not('.slick-initialized').slick({
        infinite: true,
        slidesToScroll: 1,
        arrow: true,
        adaptiveHeight: true
      });
    } else {
      if ($('.feedback-list-slick').hasClass('slick-initialized')) {
        $('.feedback-list-slick').slick('destroy');
      }
    }
  }
  sliderActivated();
  window.addEventListener("resize", function() {
    sliderActivated();
  });
  /*main-page feedback-section section*/

  /*contacts.html alerts example*/
  $('body').on('click', '.alerts-part .close-alert', function(e) {
    e.preventDefault();
    $(this).closest('.alert').hide(300);
    setTimeout(function() {
      $(this).closest('.alert').remove();
    }, 6000);
  });

  $('.show-alerts').on('click', function() {
    $('.alerts-part').toggle(300);
    setTimeout(function() {
      $('.alerts-part .close-alert').trigger('click');
    }, 5000);
  });

  // setTimeout(() => {
  //   $('.alerts-part .close-alert').trigger('click');
  // }, 5000);
  /*contacts.html alerts example*/

  /*helper modal, .follow*/

  $('body').on('click', '.helper-smart-sale', function() {
    // if ($(window).width() > 1289) {
    //
    // }
    $.magnificPopup.open({
      items: {
        src: '#helper-modal',
        type: 'inline'
      },
      callbacks: {
        beforeOpen: function() {
          if ($(window).width() > 767) {
            if ((window.location.pathname === '/promocodes-v2.html') || (window.location.pathname === '/catalog-v2.html') || (window.location.pathname === '/catalog.html')) {
              $('html, body').stop().animate({
                scrollTop: 0
              }, 500);
            }
          }
        },
        open: function() {
          if ((window.location.pathname === '/promocodes-v2.html') || (window.location.pathname === '/catalog-v2.html') || (window.location.pathname === '/catalog.html')) {
            $('.follow').addClass('active');
            $('body').addClass('body-for-helper');
            if ($(window).width() < 768) {
              $('.header-part').addClass('helper-modal-active');
            }
          }
        },
        close: function () {
          $('.follow').removeClass('active');
          $('body').removeClass('body-for-helper');
          $('.header-part').removeClass('helper-modal-active');
        }
      }
    });
  });
  /*helper modal, .follow*/

  /*init sales modal on hover*/
  tabHover();
  /*init sales modal on hover*/

  /*marks catalog*/
  $('.marks .mark-close').on('click', function() {
    $(this).closest('.mark').remove();
  });
  /*marks catalog*/

  /*table dynamic font-size*/
  // let count = 0;
  // $('.single-table tr:first-child').find('td').each(function(item, index) {
  //   count++;
  //
  //   if (count <= 6) {
  //
  //     $('.single-table *').css('font-size', '14px');
  //   } else {
  //     $('.single-table *').css('font-size', '12px');
  //   }
  // });

  /*table dynamic font-size*/
});

