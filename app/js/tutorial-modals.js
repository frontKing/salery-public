$('.smart-sale').magnificPopup({
  type: 'inline',
  preloader: true,
  midClick: true,
  closeBtnInside: true
});

// $('.helper-smart-sale').on('click', function() {
//   $.magnificPopup.open({
//     items: {
//       src: '#sales-modal',
//       type: 'inline'
//     },
//     callbacks: {
//       open: function() {
//         $('body').addClass('body-sales');
//         $("#sales-slider").slider("refresh");
//       },
//       close: function () {
//         $('body').removeClass('body-sales');
//       }
//     }
//   });
// });

$('.modal-link').magnificPopup({
  type: 'inline',
  preloader: true,
  midClick: true,
  closeBtnInside: true,
  callbacks: {
    open: function() {
      $('.body').addClass('body-for-modal');
      $("#sales-slider").slider("refresh");
    },
    close: function (){
      $('.body').removeClass('body-for-modal');
    }
  }
});

$('.login').magnificPopup({
  type: 'inline',
  preloader: true,
  midClick: true,
  closeBtnInside: true,
});

$('.registration').magnificPopup({
  type: 'inline',
  preloader: true,
  midClick: true,
  closeBtnInside: true,
});

$('.registre-item .header-list__link').magnificPopup({
  type: 'inline',
  preloader: true,
  midClick: true,
  closeBtnInside: true,
});

$('.login-item .header-list__link').magnificPopup({
  type: 'inline',
  preloader: true,
  midClick: true,
  closeBtnInside: true,
});